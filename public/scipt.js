// Form Validation
const form = document.querySelector('#my-form');

form.addEventListener('submit', (event) => {
  event.preventDefault();

  const emailInput = form.querySelector('input[type="email"]');
  const email = emailInput.value.trim();

  if (!isValidEmail(email)) {
    // Display error message
    const error = document.createElement('div');
    error.classList.add('error');
    error.textContent = 'Please enter a valid email address.';
    form.insertBefore(error, emailInput.nextSibling);
    return;
  }

  // Submit form
  form.submit();
});

function isValidEmail(email) {
  // Use regular expression to check email format
  const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return regex.test(email);
}

// Animated Effects
const elements = document.querySelectorAll('.animate');

elements.forEach((element) => {
  const position = element.getBoundingClientRect().top;
  const screenHeight = window.innerHeight;

  if (position < screenHeight) {
    element.classList.add('animated');
    element.classList.add('animated-fade-in');
  } else {
    window.addEventListener('scroll', () => {
      if (position < screenHeight) {
        element.classList.add('animated');
        element.classList.add('animated-fade-in');
        window.removeEventListener('scroll');
      }
    });
  }
});

// Dynamic Content
const input = document.querySelector('#my-input');
const output = document.querySelector('#my-output');

input.addEventListener('input', () => {
  output.textContent = input.value;
});

function animate() {
  elements.forEach((element) => {
    const position = element.getBoundingClientRect().top;
    const screenHeight = window.innerHeight;

    if (position < screenHeight) {
      element.classList.add('animated');
    }
  });
}

window.addEventListener('scroll', animate);